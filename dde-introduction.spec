%global debug_package   %{nil}
%define specrelease 2%{?dist}
%if 0%{?openeuler}
%define specrelease 2
%endif

Name:           dde-introduction
Version:        5.6.13
Release:        %{specrelease}
Summary:        Introduction for UOS
License:        GPLv3+
URL:            https://github.com/linuxdeepin/%{name}
Source0:        %{url}/archive/%{version}/%{name}-%{version}.tar.gz

BuildRequires: gcc-c++
BuildRequires: cmake3
BuildRequires: qt5-devel

BuildRequires: dtkwidget-devel
BuildRequires: pkgconfig(dtkgui)
BuildRequires: pkgconfig(dtkcore)
BuildRequires: pkgconfig(dframeworkdbus)
# BuildRequires: pkgconfig(libdmr)
BuildRequires: pkgconfig(gsettings-qt)
BuildRequires: deepin-desktop-base
BuildRequires: ffmpegthumbnailer-devel
BuildRequires: gtest-devel
BuildRequires: gmock-devel
BuildRequires: qt5-qtbase-private-devel

BuildRequires: deepin-desktop-server
Requires:      deepin-desktop-server

%description
When you log into the system for the first time, a welcome program will automatically start.
Watch the introduction video to get new features, customize your desktop, enable the window
effect and know more about UnionTech OS.

%prep
%autosetup

# disable dmr lib
sed -i 's/contains(TARGET_ARCH, x86_64)/contains(TARGET_ARCH, mips)/' introduction.pro

# fix strip
sed -i 's|gc-sections"|gc-sections -s"|' CMakeLists.txt

%build
# help find (and prefer) qt5 utilities, e.g. qmake, lrelease
export PATH=%{_qt5_bindir}:$PATH
sed -i "s|^cmake_minimum_required.*|cmake_minimum_required(VERSION 3.0)|" $(find . -name "CMakeLists.txt")
mkdir build && pushd build 
%cmake -DCMAKE_BUILD_TYPE=Release ../  -DAPP_VERSION=%{version} -DVERSION=%{version} 
%make_build  
popd

%install
%make_install -C build INSTALL_ROOT="%buildroot"
# bugfix#50424
pushd %{buildroot}%{_datadir}/dde-introduction
ln -svf server.mp4 demo.mp4
popd

%files
%doc CHANGELOG.md
%{_bindir}/%{name}
%{_datadir}/icons/hicolor/scalable/apps/%{name}.svg
%{_datadir}/%{name}/translations/*.qm
%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}/

%changelog
* Thu Mar 16 2023 liweiganga <liweiganga@uniontech.com> - 5.6.13-2
- feat: fix strip

* Mon Jul 18 2022 konglidong <konglidong@uniontech.com> - 5.6.13-1
- update version to 5.6.13

* Tue Feb 08 2022 liweigang <liweiganga@uniontech.com> - 5.6.0.7-2
- fix build error

* Fri Oct 23 2020 panchenbo <panchenbo@uniontech.com> - 5.6.0.7-1
- update to 5.6.0.7

* Thu Oct 15 2020 weidong <weidong@openeuler.org> - 5.5.6.1-4
- Fix compilation errors

* Thu Oct 15 2020 weidong <weidong@openeuler.org> - 5.5.6.1-3
- Fix compilation errors

* Thu Oct 15 2020 weidong <weidong@openeuler.org> - 5.5.6.1-2
- add buildrequires

* Thu Jul 30 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.5.6.1-1
- Package init
