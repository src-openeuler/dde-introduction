# dde-introduction

#### Description
dde introduction

#### Software Architecture
When you log into the system for the first time, a welcome program will automatically start.
Watch the introduction video to get new features, customize your desktop, enable the window
effect and know more about OS.

#### Installation

1.  dnf install dde-introduction

#### Instructions

1.  When you log in to the system for the first time, the welcome page is displayed and you can watch the OS introduction video
2. Next, set the desktop style. You can choose "Fashionable Mode" or "Efficient Mode"

3. Next, set the operating mode. You can select Special Effect mode or Common Mode

4. Next, set the icon theme. You can select four icon styles for setting.

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
